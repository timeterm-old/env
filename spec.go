package env

import (
	"fmt"
	"os"
	"reflect"
	"sort"

	"github.com/hashicorp/go-multierror"
)

var globalSpec Spec

// Spec contains all environment variables that a component requires.
type Spec []Var

// RegisterSpec registers one or more environment variable specifications
// in the global specification.
func RegisterSpec(s ...Spec) {
	for _, spec := range s {
		globalSpec = append(globalSpec, spec...)
	}
}

// GlobalSpec is the global specification of all environment variables.
func GlobalSpec() Spec {
	s := make(Spec, len(globalSpec))
	copy(s, globalSpec)

	return s
}

// ParserFunc is a function which can parse the string value of an environment
// variable into another type. If you must, you can use MustReflectParser
// for easily using a different parser function signature without writing
// a closure wrapping the function (MustReflectParser does this for you).
type ParserFunc func(string) (interface{}, error)

// Var is a basic description of an environment variable.
type Var struct {
	Name     string
	Help     string
	Value    string
	Required bool
	Parser   ParserFunc
}

// MustReflectParser takes a function with a signature like
// 	func(string) (Type, error)
// and converts it to a ParserFunc if the signature conforms. Examples which would work
// are time.ParseDuration and strconv.Atoi.
// If not, the function panics.
func MustReflectParser(p interface{}) ParserFunc {
	rv := reflect.ValueOf(p)
	if !rv.IsValid() {
		panic("invalid reflect type for parser function input")
	}

	rt := rv.Type()
	if rt.Kind() != reflect.Func {
		panic("expected parser to be a function")
	}
	if rt.NumIn() != 1 {
		panic("expected one string input argument for a parser function")
	}
	if rt.NumOut() != 2 {
		panic("expected two outputs for a parser function")
	}
	if !rt.Out(1).Implements(reflect.TypeOf((*error)(nil)).Elem()) {
		panic("expected second output to be an error")
	}
	return func(s string) (i interface{}, err error) {
		values := rv.Call([]reflect.Value{reflect.ValueOf(s)})

		// We can assert that the type assertion is successful due to the earlier
		// check on the type of the second output parameter (which we know implements error
		// if the function has made it this far).
		var outErr error
		if !values[1].IsNil() {
			outErr = values[1].Interface().(error)
		}

		return values[0].Interface(), outErr
	}
}

// Resolve retrieves all environment variables required in the specification.
// If any required environment variable is not found it is added to the list of errors which is returned after all
// environment variables have been checked. If an environment variable is not specified by the user,
// the default value will be used (specified in `Value`). If this is not specified either, an empty string
// is used and an error will be returned for the empty environment variable if it is required.
func (s Spec) Resolve() (envs map[string]string, err error) {
	envs = make(map[string]string, len(s))

	for _, env := range s {
		val, exists := os.LookupEnv(env.Name)
		if !exists {
			if env.Required {
				err = multierror.Append(err,
					fmt.Errorf("could not find required environment variable %s", env.Name),
				)
				continue
			}

			val = env.Value
		}

		envs[env.Name] = val
	}

	return envs, err
}

// ResolveParse resolves all environment variables, but also trying to parse them
// with the added Parser for each environment variable.
func (s Spec) ResolveParse() (envs map[string]interface{}, err error) {
	var strEnvs map[string]string

	strEnvs, err = s.Resolve()
	if err != nil {
		return nil, err
	}

	envs = make(map[string]interface{}, len(strEnvs))

	for _, env := range s {
		if env.Parser != nil {
			parsed, err := env.Parser(strEnvs[env.Name])
			if err != nil {
				err = multierror.Append(err,
					fmt.Errorf("could not parse environment variable %s: %w", env.Name, err),
				)
				continue
			}

			envs[env.Name] = parsed
			continue
		}

		envs[env.Name] = strEnvs[env.Name]
	}

	return envs, err
}

// PrintGlobalSpec prints all environment variables for the program in lexicographically ascending order and returns.
func PrintGlobalSpec() {
	envs := GlobalSpec()
	sort.SliceStable(envs, func(i, j int) bool {
		return envs[i].Name < envs[j].Name
	})
	for _, envVar := range envs {
		fmt.Printf("%s (required: %t, default: %q) %s\n",
			envVar.Name, envVar.Required, envVar.Value, envVar.Help)
	}
}
