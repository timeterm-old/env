env
===

env provides basic utility functions for resolving environment variables.
Multiple packages can provide and register environment variable specifications,
which contain the environment variables which are expected or strictly required
to be set, with optional default values.

Additionally, a parser function can be provided which the environment variable
must be parsed with. The resulting value is placed inside of an `interface{}` if
`env.ResolveParse` is called. Note that the parser function is _not_ called when
`env.Resolve` is used.

A parser function has the type `func(string) (interface{}, error)`, which the
function you would like to use does usually not conform to. You can use
`env.MustReflectParser` for automatically creating a wrapper, or simply create
a closure. A closure is probably less error-prone.

Example of a package requiring an environment variable:

```go
package greeter

import (
    "fmt"

    "gitlab.com/timeterm/env"
)

const envOwnName = "OWN_NAME"

func init() {
    env.RegisterSpec(GreeterEnvSpec())
}

func GreeterEnvSpec() env.Spec {
    return env.Spec {
        {
            Name: envOwnName,
            Help: "Own name which is presented to the entity greeted",
            Required: true,
        },
    }
}

func Greet(who string) {
    envs, err := GreeterEnvSpec().Resolve()
    if err != nil {
        panic(err)
    }
    ownName := envs[envOwnName]

    fmt.Printf("Hello %s, I'm %s!\n", who, ownName)
}
```