package env

import (
	"errors"
	"reflect"
	"strconv"
	"testing"
)

func TestMustReflectParser(t *testing.T) {
	tests := map[string]struct {
		function interface{}
		err      error
		input    string
		output   interface{}
		outerr   error
	}{
		"atoi": {
			function: strconv.Atoi,
			input:    "1234",
			output:   1234,
		},
		"atoi_invalid": {
			function: strconv.Atoi,
			input:    "1234abc",
			outerr:   strconv.ErrSyntax,
		},
	}

	for name := range tests {
		name := name
		tc := tests[name]

		t.Run(name, func(t *testing.T) {
			defer func() {
				if err := recover(); !reflect.DeepEqual(err, tc.err) {
					t.Fatalf("err != tc.err: err=%v, tc.err=%v", err, tc.outerr)
				}
			}()

			fn := MustReflectParser(tc.function)

			out, err := fn(tc.input)
			if !isError(err, tc.outerr) {
				t.Fatalf("outerr != tc.outerr: outerr=%v, tc.outerr=%v", err, tc.outerr)
			}
			if err != nil {
				return
			}

			if !reflect.DeepEqual(out, tc.output) {
				t.Fatalf("out != tc.output: out=%v, tc.output=%v", out, tc.output)
			}
		})
	}
}

// isError digs through structs to find an error field (e.g. with strconv.NumError, where the Unwrap method is not
// yet implemented (it will be in Go 1.14)).
func isError(err, target error) bool {
	tryFields := []string{"Err", "Error"}

	if errors.Is(err, target) {
		return true
	}

	for next := err; next != nil; next = errors.Unwrap(err) {
		err = next
	}

	rv := reflect.ValueOf(err)
	for rv.Kind() == reflect.Ptr {
		rv = rv.Elem()
		if rv.IsZero() {
			return false
		}
	}

	for _, f := range tryFields {
		field := rv.FieldByName(f)
		if !field.IsZero() && field.Type().Implements(reflect.TypeOf((*error)(nil)).Elem()) {
			return isError(field.Interface().(error), target)
		}
	}

	return false
}
